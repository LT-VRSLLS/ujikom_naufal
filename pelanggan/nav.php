<?php 
include "koneksi.php";
include "../classes/Session.php";
include "../classes/database.php";
$db = new database();
require_once('core/init.php');

?>
<div class="pos-f-t">
    <div class="collapse" id="navbarToggleExternalContent">
    <div style="background-color: #0e3d46" class="p-4">
      <h4 class="text-white" align="center">Menu</h4>
      <br>
  <div class="row">
        <div class="col col-lg-1 text-white">
           <a href="#seafood" style="color: #fff">Seafood</a>
        </div>
        <div class="col col-lg-1 text-white">
           <a href="#" style="color: #fff">Breakfast</a>
        </div>
        <div class="col col-lg-1 text-white">
           <a href="#" style="color: #fff">Lunch</a>
        </div>
        <div class="col col-lg-1 text-white">
           <a href="#japanese" style="color: #fff">Japanese</a>
        </div>  
        <div class="col col-lg-1 text-white">
           <a href="#" style="color: #fff">Juice</a>
        </div>      
   </div>  
  </div>
  </div>
<nav style="background-color: #174b56" class="navbar navbar-expand-lg" id="navbar-example">
  <a class="navbar-brand" href="#" style="color: #fff">RestToRun</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span style="color: #fff" class="navbar-toggler-icon">V</span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
     <li>
<button type="button" class="btn btn-outline-info" data-toggle="modal" data-target=".bd-pesanan-modal-lg">Pesanan</button>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="menu.php" style="color: #fff" data-toggle="collapse"  data-target="#navbarToggleExternalContent"aria-controls="navbarToggleExternalContent">Menu</a>
      </li>
       <li class="nav-item active">
        <a  href="../pelayan/index.php" class="nav-link" style="color: #fff">Kembali</a>
      </li>
    </ul>

<div class="modal fade bd-pesanan-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pesanan anda</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form role="form" action="index.php" method="post" enctype="multipart/form-data">
                  <div class="card-body ">
                    <table border="2" class="table">
          <thead>
          <tr>
            <th>Nama pelanggan</th>
            <th>Nama pelayan</th>
            <th>Nama masakan</th>
            <th>Harga masakan</th>
            <th>Jumlah masakan</th>
            <th>Ubah/Hapus</th>
          </tr>
        </thead>
                  <?php
          $id_order=input::get('id_order');
          $look="SELECT * from temp_order t 
                                          left join tb_order o on o.id_order=t.id_order
                                          left join users u on u.id=t.id_user
                                          left join masakan m on m.id_masakan=t.id_masakan
          where t.id_order='$id_order'";
          $watch=mysqli_query($db->mysqli,$look);
          while($see=mysqli_fetch_array($watch)){
          ?>
           <tbody>
          <tr>
            <td><?php echo $see['keterangan'];?></td>
            <td><?php echo $see['nama_user'];?></td>
            <td><?php echo $see['nama_masakan'];?></td>
            <td><?php echo $see['harga'];?></td>
            <td><?php echo $see['jumlah_masakan'];?></td>
            <td><a href="index.php?id_temp_order=<?php echo $see['id_temp_order'];?>&&id_order=<?php echo $id_order;?>" class="btn btn-danger">Hapus</a></td>
<input type="hidden" value="<?php echo $id_order;?>" name="id_order[]">
<input type="hidden" value="<?php echo $id_order;?>" name="hapus_order">
<input type="hidden" value="<?php echo $see['id_masakan'];?>" name="id_masakan[]">
<input type="hidden" value="<?php echo $see['keterangan'];?>" name="keterangan[]">
<input type="hidden" value="<?php echo $see['jumlah_masakan'];?>" name="jumlah_masakan[]">
          </tr>
          </tbody>
        <?php }?>
        </table>
        <?php
         $id_order=input::get('id_order');
          $look="SELECT * from temp_order where id_order='$id_order'";
          $watch=mysqli_query($db->mysqli,$look);
          $cek=mysqli_num_rows($watch);
        if($cek>0){
        ?>
      <div class="modal-footer">
         <input type="submit" value="Buat" name="semua" class="btn btn-outline-dark"/>
     </div>
   <?php }?>
 </div> 
</form>
      </div>

    </div>
  </div>
</div>

<!--      form search   <div class="text-right">
  <form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
</div> -->
  </div>
</nav>
</div>