<!DOCTYPE html>
<html>
<?php
	include "header.php";
?>
<body>
<?php
	include "nav.php";
?>

<div class="jumbotron">
  <h1 class="display-3">What are you looking for?</h1>
  <p class="lead">Food?.</p>
</div>

<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <th></th>
          </tr>
        </table>
      </div>

    </div>
  </div>
</div>
  <?php 
  require_once('core/init.php');
  if(input::get('submit')){

    $user->input_order(array(
          'nama_masakan' => input::get('nama_masakan'),
          'harga' => input::get('harga'),
          'img' => input::get('img'),
          'status_masakan' => input::get('status_masakan'),

        ));
  }

  ?>
                        
                        <form action="search.php" method="post">
                        <?php
                        $select="SELECT * FROM masakan";
                        $query=mysqli_query($db->mysqli,$select);
                        while($data=mysqli_fetch_array($query)){
                        ?>
<div class="card" style="width: 20rem;margin-left: 20px;">
  <img class="card-img-top" src="../img/<?php echo $data['img'];?>" alt="Card image cap">
  <div class="card-body">
    <h4 class="card-title"><?php echo $data['nama_masakan'];?></h4>
    <p class="card-text"><?php echo $data['status_masakan'];?></p>
 <div class="col-lg-12">
    <div class="input-group">
      <input type="number" class="form-control" name="jumlah_masakan" />
      <span class="input-group-btn">
        <button class="btn btn-primary" type="submit" name="submit">Tambah</button>
      </span>
    </div>
  </div>
  </div>
</div>
</form>
<?php }?>
<br>
</body>
<?php
	include "footer.php";
?>
</html>