<?php 
include "validasi.php";
?>
<!DOCTYPE html>
<html>
<?php
include "header.php";
?>
<body>
  <!-- Nav -->
<?php include "nav.php"; ?>
<!-- /Nav -->
<hr>
<h2 align="center">List Transaksi</h2>
<hr>
<table class="table table-striped table-bordered" id="myTable">
	 	 <thead>
                       <tr>
                          <th>Id Order</th>
                          <th>Nama Pelayan</th>
                          <th>Waktu</th>
                          <th>No meja</th>
                          <th>Status Order</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $select="SELECT * FROM tb_order t
                                      left join users u on u.id=t.id_user;
                                      ";
                        $query=mysqli_query($db->mysqli,$select);
                        while($data=mysqli_fetch_array($query)){;
                        ?>
                      	<tr>
                      		<td><?php echo $data['id_order'];?></td>
                          <td><?php echo $data['nama_user'];?></td>
                          <td><?php echo $data['tanggal'];?></td>
                          <td><?php echo $data['no_meja'];?></td>
                      		<td><?php echo $data['status_order'];?></td>
                      		<td><a href="#myBayar?id_order=<?php echo $data['id_order'];?>"><button type="button" class="btn btn-outline-info" data-toggle="modal"
                            data-target="#myBayar<?php echo $data['id_order'];?>">Bayar</button></a></td>
                      	</tr>
                      <?php };?>

                      	</tbody>
                      </table>
<br>
<?php
          $id_order=$_GET['id_order'];
          $look="SELECT * from detail_order t
                                          left join tb_order o on o.id_order=t.id_order
                                          left join users u on u.id=o.id_user
                                          left join masakan m on m.id_masakan=t.id_masakan";
          $watch=mysqli_query($db->mysqli,$look);
          while($see=mysqli_fetch_array($watch)){;
          ?>
<div class="modal fade" id="myBayar<?php echo $see['id_order'];?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pembayaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <form role="form" action="index.php" method="post" enctype="multipart/form-data">
                  <div class="card-body ">
                    <table border="2" class="table" id="myTable">
          <tr>
            <th>Nama pelanggan</th>
            <th>Nama pelayan</th>
            <th>Nama masakan</th>
            <th>Harga masakan</th>
            <th>Jumlah masakan</th>
          </tr>
        </thead>
         <tbody>
          <tr>
            <td><?php echo $see['keterangan'];?></td>
            <td><?php echo $see['nama_user'];?></td>
            <td><?php echo $see['nama_masakan'];?></td>
            <td><?php echo $see['harga'];?></td>
            <td><?php echo $see['jumlah_masakan'];?></td>
          </tr>
          </tbody>
        </table>
<?php };?>
      <div class="modal-footer">
        <a href="cetak.php" target="_BLANK" class="btn btn-outline-info">Print</a>
     </div>
 </div> 
</form>
      </div>

    </div>
  </div>
</div>
</body><?php require_once "footer.php"; ?>
</html>