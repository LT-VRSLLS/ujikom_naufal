<?php
include "../classes/database.php";
include "../classes/Session.php";
$db = new database();
session_start();
  if(!Session::exists('id_admin')){
    header('location:../index.php');
  }
?>
<html>
  <head>
    <title>Data Transaksi | Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
     <link href="js/jquery.dataTables.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
  </head>
<body>
<!-- Edit User -->
<?php 
include "nav.php"
?>
<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
	<hr>
	 <table class="table table-striped table-bordered" id="myTable">
	 	 <thead>
                        <tr>
                          <th>Id Transaksi</th>
                          <th>Id Order</th>
                          <th>Masakan</th>
                          <th>Nama Pelayan</th>
                          <th>Tanggal</th>
                          <th>Total Bayar</th>
                          <th>Status Order</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $select="SELECT * FROM transaksi t 
                        LEFT JOIN tb_order o ON o.id_order=t.id_order
                        LEFT JOIN users u ON u.id=t.id_user
                        LEFT JOIN masakan m ON m.id_masakan=o.id_order";
                        $query=mysqli_query($db->mysqli,$select);
                        while($data=mysqli_fetch_array($query)){
                        ?>
                      	<tr>
                      		<td><?php echo $data['id_transaksi'];?></td>
                      		<td><?php echo $data['id_order'];?></td>
                      		<td><?php echo $data['nama_masakan'];?></td>
                          <td><?php echo $data['nama_user'];?></td>
                          <td><?php echo $data['tanggal'];?></td>
                      		<td>RP. <?php echo number_format($data['total_bayar'])?></td>
                      		<td><?php echo $data['status_order'];?></td>
                      		<td></td>
                      	</tr>
                        <div class="modal fade" id="ModalEdit<?php echo $data['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php
        require_once('core/init.php');

        if(input::get('Edit')){
            $id_user=$_POST['id'];
            $username=$_POST['username'];
            $email=$_POST['email'];
            $nama_user=$_POST['nama_user'];
            $id_level=$_POST['id_level'];
            $update=mysqli_query($db->mysqli,"UPDATE users SET username='$username',email='$email',nama_user='$nama_user',id_level='$id_level' where id='$id_user'");

        }
        ?>
        <form action="user.php" method="post">
            <input type="hidden" class="form-control" name="id" value="<?php echo $data['id'];?>">
          <div class="form-group">
            <label class="col-form-label">Username:</label>
            <input type="text" class="form-control" name="username" value="<?php echo $data['username'];?>">
          </div>
          <div class="form-group">
            <label class="col-form-label">Email:</label>
            <input type="text" class="form-control" name="email" value="<?php echo $data['email'];?>">
          </div>
          <div class="form-group">
            <label class="col-form-label">Nama:</label>
            <input type="text" class="form-control" name="nama_user" value="<?php echo $data['nama_user'];?>">
          </div>
          <div class="form-group">
            <label class="col-form-label">Level:</label>
            <select name="id_level" class="custom-select">
              <option value="1">Pelanggan</option>
              <option value="2">Kasir</option>
              <option value="3">Pelayan</option>
              <option value="4">owner</option>
              <option value="5">administrator</option>
                </select>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <input type="submit" class="btn btn-primary" name="Edit"/>
      </div>
        </form>
      </div>
    </div>
  </div>
</div>
    <?php }?>
   </tbody>
	 </table>
<?php 
define('footer', true);
include "footer.php" ?>
</main>
</body>
</html>