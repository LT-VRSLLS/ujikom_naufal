      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="index.html#">Dashboard</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="setting.php">Settings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="Profile">Profile</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="Help">Help</a>
            </li>
          </ul>
           <a href="logout.php" class="btn btn-outline-light">Log Out</a>
        </div>

      </nav>

              <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
          <ul class="nav nav-pills flex-column">
            <li class="nav-item">
              <a class="nav-link" href="user.php">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="masakan.php">Masakan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="data_transaksi.php">Data Transaksi</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="laporan">Laporan</a>
            </li>
          </ul>
        </nav>