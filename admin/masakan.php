<?php
include "../classes/database.php";
include "../classes/Session.php";
$db = new database();
session_start();
  if(!Session::exists('id_admin')){
    header('location:../index.php');
  }
?>
<html>
  <head>
    <title>Masakan | Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
     <link href="js/jquery.dataTables.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-grid.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-reboot.css">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
  </head>
<body>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Masakan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<?php
      	require_once('core/init.php');

      	if(input::get('submit')){

      	$user->input_masakan(array(
      		'nama_masakan' => input::get('nama_masakan'),
          'harga' => input::get('harga'),
          'img' => input::get('img'),
          'status_masakan' => input::get('status_masakan'),

      	));

        Session::set('nama_masakan',input::get('nama_masakan') );
        header('location:masakan.php');
      	}

           if(input::get('id_hapus')){
            $id= $_GET['id_hapus'];
             mysqli_query($db->mysqli,"DELETE from masakan where id_masakan = '$id'");
            header('location:masakan.php');
          }
      	?>
        <form action="masakan.php" method="post">
          <div class="form-group">
            <label class="col-form-label">Nama Masakan</label>
            <input type="text" class="form-control" name="nama_masakan">
          </div>
          <div class="form-group">
            <label class="col-form-label">Harga</label>
            <input type="text" class="form-control" name="harga">
          </div>
          <div class="form-group">
            <label class="col-form-label">Foto:</label>
            <input type="file" class="form-control" name="img">
          </div>
          <div class="form-group">
            <label class="col-form-label">Status Masakan:</label>
            <select name="status_masakan" class="custom-select">
              <option value="ada">Ada</option>
              <option value="tidak ada">Tidak ada</option>
              
            </select>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <input type="submit" class="btn btn-primary" name="submit"/>
      </div>
        </form>
      </div>

    </div>
  </div>
</div>
<?php 
include "nav.php"
?>
<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">

	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Tambah</button>
	<hr>
	 <table class="table table-striped table-bordered" id="myTable">
	 	 <thead>
                        <tr>
                          <th>Id masakan</th>
                          <th>Nama masakan</th>
                          <th>harga</th>
                          <th>Gambar</th>
                          <th>Status</th>

                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                         <?php
                        $select="SELECT * FROM masakan";
                        $query=mysqli_query($db->mysqli,$select);
                        while($data=mysqli_fetch_array($query)){
                        ?>
                      	<tr>
                      		<td><?php echo $data['id_masakan'];?></td>
                      		<td><?php echo $data['nama_masakan'];?></td>
                      		<td>Rp.<?php echo number_format($data['harga'])?></td>
                      		<td><img src="../img/<?php echo $data['img'];?>" width="300"></td>
                      		<td><?php echo $data['status_masakan'];?></td>
                      		<td><a href="masakan.php?id_hapus=<?php echo $data['id_masakan'];?>" class="btn btn-danger">Hapus</a> 
                            | <a href="#" class="btn btn-success" data-toggle="modal" data-target="#ModalEdit<?php echo $data['id_masakan'];?>">Edit</a>
                          </td>
                        </tr>
                        <div class="modal fade" id="ModalEdit<?php echo $data['id_masakan'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Masakan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php
        require_once('core/init.php');

        if(input::get('edit')){
            $id_masakan=$_POST['id_masakan'];
            $nama_masakan=$_POST['nama_masakan'];
            $harga=$_POST['harga'];
            $update=mysqli_query($db->mysqli,"UPDATE masakan SET nama_masakan='$nama_masakan',harga='$harga' where id_masakan='$id_masakan'");
            if($update){
              echo "<script>window.location.href='masakan.php'</script>";              
            }
        }
        ?>
        <form action="masakan.php" method="post">
            <input type="hidden" class="form-control" name="id_masakan"
             value="<?php echo $data['id_masakan'];?>">
          <div class="form-group">
            <label class="col-form-label">Nama Masakan:</label>
            <input type="text" class="form-control" name="nama_masakan" value="<?php echo $data['nama_masakan'];?>">
          </div>
          <div class="form-group">
            <label class="col-form-label">Harga:</label>
            <input type="text" class="form-control" name="harga" value="<?php echo $data['harga'];?>">
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <input type="submit" class="btn btn-primary" name="edit"/>
      </div>
        </form>
      </div>
    </div>
  </div>
</div>
   <?php }?>
   </tbody>
	 </table>
<?php
define('footer', true);
 include "footer.php" ?>
</main>
</body>
</html>