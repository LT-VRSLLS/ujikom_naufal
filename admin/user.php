<?php
include "../classes/database.php";
include "../classes/Session.php";
$db = new database();
session_start();
  if(!Session::exists('id_admin')){
    header('location:../index.php');
  }
?>
<html>
  <head>
    <title>Users | Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
     <link href="js/jquery.dataTables.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
  </head>
<body>
  <!-- input user -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<?php
        require_once('core/init.php');
      	   if(input::get('submit')){

      	$user->register_user(array(
      		'username' => input::get('username'),
      		'password' => password_hash(input::get('password'), PASSWORD_DEFAULT),
          'email'    => input::get('email'),
      		'nama_user' => input::get('nama_user'),
      		'id_level' => input::get('id_level')
      	));

        Session::set('username',input::get('username') );
        header('location:user.php');
      	}

        if(input::get('id')){
            $id= $_GET['id'];
             mysqli_query($db->mysqli,"DELETE from users where id = '$id'");
            header('location:user.php');
          }
      	?>
        <form action="user.php" method="post">
          <div class="form-group">
            <label class="col-form-label">Username:</label>
            <input type="text" class="form-control" name="username">
          </div>
          <div class="form-group">
            <label class="col-form-label">Password:</label>
            <input type="password" class="form-control" name="password">
          </div>
          <div class="form-group">
            <label class="col-form-label">Email:</label>
            <input type="text" class="form-control" name="email">
          </div>
          <div class="form-group">
            <label class="col-form-label">Nama:</label>
            <input type="text" class="form-control" name="nama_user">
          </div>
          <div class="form-group">
            <label class="col-form-label">Level:</label>
            <select name="id_level" class="custom-select">
            	<?php
            	$select="SELECT * FROM level";
            	$query=mysqli_query($db->mysqli,$select);
            	while($data=mysqli_fetch_array($query)){
            	?>
            	<option value="<?php echo $data['id_level'];?>">
            		<?php echo $data['nama_level']; ?>
            	</option>
            <?php }?>
            </select>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <input type="submit" class="btn btn-primary" name="submit"/>
      </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Edit User -->
<?php 
include "nav.php"
?>
<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">

	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Tambah</button>
	<hr>
	 <table class="table table-striped table-bordered" id="myTable">
	 	 <thead>
                        <tr>
                          <th>Id user</th>
                          <th>Username</th>
                          <th>Password</th>
                          <th>Email</th>
                          <th>Nama User</th>
                          <th>Nama Level</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $select="SELECT * FROM users u LEFT JOIN level l ON l.id_level=u.id_level";
                        $query=mysqli_query($db->mysqli,$select);
                        while($data=mysqli_fetch_array($query)){
                        ?>
                      	<tr>
                      		<td><?php echo $data['id'];?></td>
                      		<td><?php echo $data['username'];?></td>
                      		<td><?php echo $data['password'];?></td>
                          <td><?php echo $data['email'];?></td>
                      		<td><?php echo $data['nama_user'];?></td>
                      		<td><?php echo $data['nama_level'];?></td>
                      		<td><a href="user.php?id=<?php echo $data['id'];?>" class="btn btn-danger">Hapus</a> 
                            | <a href="#" class="btn btn-success" data-toggle="modal" data-target="#ModalEdit<?php echo $data['id'];?>">Edit</a>
                          </td>
                      	</tr>
                        <div class="modal fade" id="ModalEdit<?php echo $data['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php
        require_once('core/init.php');

        if(input::get('Edit')){
            $id_user=$_POST['id'];
            $username=$_POST['username'];
            $email=$_POST['email'];
            $nama_user=$_POST['nama_user'];
            $id_level=$_POST['id_level'];
            $update=mysqli_query($db->mysqli,"UPDATE users SET username='$username',email='$email',nama_user='$nama_user',id_level='$id_level' where id='$id_user'");

        }
        ?>
        <form action="user.php" method="post">
            <input type="hidden" class="form-control" name="id" value="<?php echo $data['id'];?>">
          <div class="form-group">
            <label class="col-form-label">Username:</label>
            <input type="text" class="form-control" name="username" value="<?php echo $data['username'];?>">
          </div>
          <div class="form-group">
            <label class="col-form-label">Email:</label>
            <input type="text" class="form-control" name="email" value="<?php echo $data['email'];?>">
          </div>
          <div class="form-group">
            <label class="col-form-label">Nama:</label>
            <input type="text" class="form-control" name="nama_user" value="<?php echo $data['nama_user'];?>">
          </div>
          <div class="form-group">
            <label class="col-form-label">Level:</label>
            <select name="id_level" class="custom-select">
              <option value="1">Pelanggan</option>
              <option value="2">Kasir</option>
              <option value="3">Pelayan</option>
              <option value="4">owner</option>
              <option value="5">administrator</option>
                </select>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <input type="submit" class="btn btn-primary" name="Edit"/>
      </div>
        </form>
      </div>
    </div>
  </div>
</div>
    <?php }?>
   </tbody>
	 </table>
<?php 
define('footer', true);
include "footer.php" ?>
</main>
</body>
</html>