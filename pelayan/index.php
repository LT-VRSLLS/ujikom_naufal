<?php 
include "../classes/Session.php";
include "../classes/database.php";
$db = new database();
session_start();
  if(!Session::exists('id_pelayan')){
    header('location:../index.php');
  }
?>
<!DOCTYPE html>
<html>
<?php include "header.php";?>
<body>
	<?php
	require_once('core/init.php');
	if(input::get('buat')){
	
		$user->input_pesanan(array(
      		'no_meja' => input::get('no_meja'),
      		'id_user' => input::get('id_user'),
      		'tanggal' => date("Y-m-d G:i:s", time()+50*60*6),
      		'keterangan' => input::get('keterangan'),
      		'status_order' => input::get('status_order')

      	));
        Session::set('id_order',input::get('id_order') );
        Session::set('no_meja',input::get('no_meja') );
        Session::set('keterangan', input::get('keterangan'));
        Session::set('id_user', input::get('id_user'));
        header('location:index.php');
      	 
	}

	?>
	<?php include "nav.php";?>
  <div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-3">You're working right? do something <?php echo $_SESSION['nama'];?>!!!</h1>
    <p class="lead">Serve our customer like they are our king! like there is no tomorrow bcuz no one promise tomorrow.</p>
  </div>
</div>
</body>
<footer>
	<?php include "footer.php";?>
</footer>
</html>