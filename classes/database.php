<?php
 
 	class database {

 		private static $INSTANCE;
 		public $mysqli,
 				$HOST = 'localhost',
 				$USER = 'root',
 				$PASS = '',
 				$DB   = 'friday';

 	public function __construct(){

 		$this->mysqli = new mysqli($this->HOST,$this->USER,$this->PASS,$this->DB);
 	}

 	public static function getInstance(){
 		if (!isset (self::$INSTANCE)){
 			self::$INSTANCE = new database();
 		}
 		return self::$INSTANCE;
 	}

 	public function insert($table,$fields = array()){

 		$kolom=implode(", ",array_keys($fields));

 		$valueArrays=array();
 		$i=0;
 		foreach($fields as $key=>$values){
 			if(is_int($values)){
 				$valueArrays[$i] = $this->escape($values);
 			}else{
 				$valueArrays[$i] = "'" . $this->escape($values) . "'";
 			}
 			$i++;
 		}

 		$values=implode(", ",$valueArrays);

 		$query="INSERT INTO $table ($kolom) VALUES ($values)";

 		return $this->run_query($query, 'Masalah saat memasukan data');
 	}


 	public function get_info($table,$coloumn,$value){

 		if(!is_int($value)) $value = "'" .$value. "'";


 		$query  = "SELECT * FROM $table WHERE $coloumn = $value";
 		$result = $this->mysqli->query($query);
 		while ($row = $result->fetch_assoc()){
 			return $row;
 		}
 	}



 	public function run_query($query, $msg){
 		if($this->mysqli->query($query)) return true;
 		else return false;
 	}

 	public function escape($name){
 		return $this->mysqli->real_escape_string($name);
 	}

	}
 ?>