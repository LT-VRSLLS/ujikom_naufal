<?php

class input_user{

	private $_db;

	public function __construct(){
		$this->_db = database::getInstance();
	}

	public function register_user($fields = array()){
		if($this->_db->insert('users', $fields) ) return true;
		else return false;
	}

	public function input_masakan($fields = array()){
		if($this->_db->insert('masakan', $fields) ) return true;
		else return false;
	}

	public function input_pesanan($fields = array()){
		if($this->_db->insert('tb_order', $fields) ) return true;
		else return false;
	}
	public function input_order($fields = array()){
		if($this->_db->insert('temp_order', $fields) ) return true;
		else return false;
	}
	public function konfirmasi_pesanan($fields = array()){
		if($this->_db->insert('detail_order', $fields) ) return true;
		else return false;
	}

	public function login_user($username,$password){
		$data = $this->_db->get_info('users','username',$username);
		
		if(password_verify($password,$data['password']))
			return true;
		else return false;
	}

}

?>